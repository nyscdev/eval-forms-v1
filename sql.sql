-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2018 at 12:23 PM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intelpro_nysceva`
--
CREATE DATABASE IF NOT EXISTS `intelpro_nysceva` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intelpro_nysceva`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `usr` varchar(40) DEFAULT NULL,
  `psw` varchar(40) DEFAULT NULL,
  `priv` varchar(20) DEFAULT NULL,
  `createdby` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ansQ`
--

CREATE TABLE `ansQ` (
  `stateCode` varchar(12) NOT NULL,
  `formname` varchar(100) NOT NULL,
  `section` varchar(100) NOT NULL,
  `queNo` int(2) NOT NULL,
  `ansSupplied` varchar(250) NOT NULL,
  `dateComputed` date NOT NULL,
  `timComputed` time NOT NULL,
  `datUpdated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `corpsmembers`
--

CREATE TABLE `corpsmembers` (
  `statecodeno` varchar(12) NOT NULL,
  `callupno` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `surname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `FORM4A`
--

CREATE TABLE `FORM4A` (
  `id` int(9) NOT NULL,
  `ques` varchar(254) DEFAULT NULL,
  `opta` varchar(200) DEFAULT NULL,
  `optb` varchar(200) DEFAULT NULL,
  `optc` varchar(200) DEFAULT NULL,
  `optd` varchar(200) DEFAULT NULL,
  `part` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `formparts`
--

CREATE TABLE `formparts` (
  `id` int(9) NOT NULL,
  `formname` varchar(30) DEFAULT NULL,
  `formpart` varchar(30) DEFAULT NULL,
  `partdesc` varchar(100) DEFAULT NULL,
  `usr` varchar(50) DEFAULT NULL,
  `dat` varchar(50) DEFAULT NULL,
  `usr_writing` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(9) NOT NULL,
  `formname` varchar(30) DEFAULT NULL,
  `formdesc` varchar(100) DEFAULT NULL,
  `usr` varchar(50) DEFAULT NULL,
  `dat` varchar(50) DEFAULT NULL,
  `usr_writing` varchar(100) DEFAULT NULL,
  `lockprotocol` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansQ`
--
ALTER TABLE `ansQ`
  ADD KEY `ansq_idx_section_queno_anssupplie` (`section`,`queNo`,`ansSupplied`);

--
-- Indexes for table `FORM4A`
--
ALTER TABLE `FORM4A`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formparts`
--
ALTER TABLE `formparts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `FORM4A`
--
ALTER TABLE `FORM4A`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `formparts`
--
ALTER TABLE `formparts`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;